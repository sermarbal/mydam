# Explicació del projecte

### Important

Conexió al servidor labs.iam.cat per a guardar dades a la base de dades. Fa servir PHPs per a interactuar mitjançant clases WWW originades per unity.
Executar en resolució 1280x1024 per al correcte funcionament. Unity fa servir uns anclatjes per a poder escalar els elements del menu però no s'han implementat en aquest projecte. S'ha intentat fer d'un altra mode, escalant a partir del canvas pare.

### Parts del joc

El joc té dues parts principals, el menú i el joc en si.
El menu conté tots els menus i el codi que treballa amb la interacció de l'usuari excepte el menu final de cada joc que està implementat dintre del joc.
El joc conté una pantalla sentzilla però en la que s'utilitzen diferents parts de codi de unity per a fer els events originats durant la partida.

### Encapsalament public i privat

Hi ha moltes variables publiques a les clases perquè unity fa servir la seva interfície per igualar-les a un valor concret.

### Plataformes pensades:

- Mac
- Linux
- Windows

## Managers

Son les clases gestores del joc. Son estàtiques i a diferencia de la ScreenManager totes estàn presents a tots els apartats del joc.

- DAOManager: Manager que conté funcions que retornen clases WWW amb els formularis pertinents per a poder fer les connexions amb els fitxers php i així treballar amb la base de dades mysql.
- PlayerManager: Manager que conté les variables importants del jugador que ha de mantenir per la correcte gestió d'algunes parts del joc.
- SoundManager: Manager que conté metodes i gestiona els efectes i la musica del joc.
- ScreenManager: Manager que gestiona gran part del joc. (Quan s'està jugant)
- MessageManager: Manager que permet reproduir missatges per a que els llegeixi l'usuari.

## Menu

El menu està compòs de 8 canvas:

### LoginRegister

Canvas on es logueja l'usuari i es registra. Pot entrar com a Anonim per a testejar.

També conté la notificació de copyright a peu de pantalla.

### Main

Canvas principal on surten les opcions:

- Jugar: Entra a escogir la dificultat.
- Options: Permet manegar el volum del so i la música.
- Scores: Mostra les puntuacions.
- Loggout: Desconecta al jugador actual.

### Difficulty

Permet escollir la dificultat en la que es jugarà el joc.

- Easy
- Normal
- Hard

### Mode

Permet escollir el mode de joc.

- History: Cada cop que s'aconsegueix pasar un nivell obre el següent.
- Lliure: Permet anar a tots els nivells.

### Levels

Mostra tots els nivells del joc.

### Options

Permet pujar o baixar el volum de la música o effectes del joc.

### Scores

Mostra les puntuacions globals del joc. Aquestes son les màximes puntuacions dels jugadors de cada pantalla sumades.

### Exit

Dialog que pregunta al jugador si de veritat vol sortir.


## Game

### Joc general

Game te moltes miniclases que actuen com a comportaments dels diferents objectes utilitzant triggers i colliders principalment.

### Menu final del joc

Conté 3 canvas posibles:

- Guanyador: El jugador ha guanyat i se li mostra les opcions d'haver guanyat.
- Perdedor: El jugador ha perdut i se li mostra les opcions d'haver perdut.
- Info: El jugador està jugant i se li mostra el temps i les estrelles que ha conseguit recolectar.

## Ruta del joc

El jugador comença al menu LoginRegister on es pot registrar i loguejar, si ho destija pot loguejar-se com a anonymous. Si no hi ha connexió a internet es notificarà cada 10 segons degut a que els scores es van actualitzant cada 10 segons. Al haver-se loguejat rebrà un missatge de benvinguda amb el seu nom d'usuari i anirà al menu on per jugar haurà de clicar a Play i escollir una dificultat i modalitat de joc. Seguidament del nivell permès per a jugar. Al fer clic al botó del nivell es comença a jugar.

Al joc s'ha de superar un conjunt d'obstacles intentant recollir totes les estrelles possibles i arribar a la meta en el menor temps possible. 

En cas que el jugador arribi a la meta es revisarà el temps i les estrelles i la difficultat escollida per a fer la puntuació total i es mostrarà el canvas winner permeten guardar la puntuació si es desitja i en cas que s'haguès escollit la modalitat history s'intentarà insertar la relació del jugador amb la pantalla realitzada per a poder anar a la següent.

En cas que el jugador mori de caiguda o de temps o bé de colisió amb un enemic (cuadrats vermells) se li mostrarà el canvas loser on li permetrà escollir entre tornar al menu principal o bé reiniciar el joc actual.

Al sortir del joc i arribar al menu es revisarà si l'usuari està loguejat i es mostrarà el canvas de nivells si ho està.