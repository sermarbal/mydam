DROP TABLE IF EXISTS Score;
DROP TABLE IF EXISTS GamesDone;
DROP TABLE IF EXISTS user_skin;
DROP TABLE IF EXISTS Skin;
DROP TABLE IF EXISTS GameScreen;
DROP TABLE IF EXISTS User;

use a14sermarbal_dbBall;

CREATE TABLE IF NOT EXISTS User(
id int PRIMARY KEY AUTO_INCREMENT,
username varchar(16) unique not null,
password varchar(255) not null
);

/* Taula que guarda les possibles partides. */
CREATE TABLE IF NOT EXISTS GameScreen(
id int PRIMARY KEY AUTO_INCREMENT,
name varchar(60)
);

/* Taula que guarda totes les puntuacions de  */
CREATE TABLE IF NOT EXISTS Score(
id int PRIMARY KEY AUTO_INCREMENT,
score bigint not null,
idUser int,
idGameScreen int,
FOREIGN KEY (idUser) REFERENCES User(id),
FOREIGN KEY (idGameScreen) REFERENCES GameScreen(id)
);

/* Taula que guardará si una partida s'ha completat o no. (Si es troba la relació es que s'ha completat amb éxit)*/
CREATE TABLE IF NOT EXISTS GamesDone(
idUser int,
idGame int,
PRIMARY KEY(idUser, idGame),
FOREIGN KEY (idUser) REFERENCES User(id),
FOREIGN KEY (idGame) REFERENCES GameScreen(id)
);

CREATE TABLE IF NOT EXISTS Skin(
id int PRIMARY KEY AUTO_INCREMENT,
name varchar(60) not null,
price decimal(10,2) not null
/*skinFile BLOB not null, */
);

CREATE TABLE IF NOT EXISTS user_skin(
idUser int,
idSkin int,
usingSkin boolean not null default 0,
PRIMARY KEY (idUser, idSkin),
FOREIGN KEY (idUser) REFERENCES User(id),
FOREIGN KEY (idSkin) REFERENCES Skin(id)
);


/* User inserts */
INSERT INTO User (username, password)
VALUES ("Anonymous", MD5("1234"));

INSERT INTO User (username, password)
VALUES ("user2", MD5("1234"));

INSERT INTO User (username, password)
VALUES ("user3", MD5("1234"));

INSERT INTO User (username, password)
VALUES ("user4", MD5("1234"));

INSERT INTO User (username, password)
VALUES ("user5", MD5("1234"));

/* GameScreen inserts */
INSERT INTO GameScreen (name)
VALUES ("Level 1");

INSERT INTO GameScreen (name)
VALUES ("Level 2");

INSERT INTO GameScreen (name)
VALUES ("Level 3");

INSERT INTO GameScreen (name)
VALUES ("Level 4");

INSERT INTO GameScreen (name)
VALUES ("Level 5");

INSERT INTO GameScreen (name)
VALUES ("Level 6");

INSERT INTO GameScreen (name)
VALUES ("Level 7");

INSERT INTO GameScreen (name)
VALUES ("Level 8");

INSERT INTO GameScreen (name)
VALUES ("Level 9");

INSERT INTO GameScreen (name)
VALUES ("Level 10");

/* GamesDone inserts */
INSERT INTO GamesDone (idUser, idGame)
VALUES (1,1);

INSERT INTO GamesDone (idUser, idGame)
VALUES (1,2);

INSERT INTO GamesDone (idUser, idGame)
VALUES (1,3);

INSERT INTO GamesDone (idUser, idGame)
VALUES (1,4);

/* Score inserts */
INSERT INTO Score (score, idUser, idGameScreen)
VALUES (360, 1, 1);

INSERT INTO Score (score, idUser, idGameScreen)
VALUES (280, 1, 2);

INSERT INTO Score (score, idUser, idGameScreen)
VALUES (50, 1, 3);

INSERT INTO Score (score, idUser, idGameScreen)
VALUES (100, 2, 1);

INSERT INTO Score (score, idUser, idGameScreen)
VALUES (200, 2, 2);

INSERT INTO Score (score, idUser, idGameScreen)
VALUES (350, 3, 2);

/* Skin inserts */

INSERT INTO Skin (name, price)
VALUES ("Skin 1", 19.99);

INSERT INTO Skin (name, price)
VALUES ("Skin 2", 4.99);

INSERT INTO Skin (name, price)
VALUES ("Skin 3", 9.99);

INSERT INTO Skin (name, price)
VALUES ("Skin 4", 19.99);

INSERT INTO Skin (name, price)
VALUES ("Skin 5", 14.99);

/* Guardant les skins a la bbdd */
/*
INSERT INTO Skin (name, skinFile)
VALUES ("Skin 6", FILE_READ('skins/Skin6'));

INSERT INTO Skin (name, skinFile)
VALUES ("Skin 7", FILE_READ('skins/Skin7'));
*/
/* user_skins */

INSERT INTO user_skin (idUser, idSkin, usingSkin)
VALUES (1, 1, false);

INSERT INTO user_skin (idUser, idSkin, usingSkin)
VALUES (2, 2, false);

INSERT INTO user_skin (idUser, idSkin, usingSkin)
VALUES (2, 3, false);

INSERT INTO user_skin (idUser, idSkin, usingSkin)
VALUES (4, 1, false);

INSERT INTO user_skin (idUser, idSkin, usingSkin)
VALUES (5, 5, false);