﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoginRegisterTest : MonoBehaviour
{

	public static string user = "";
	private string password = "", rePass = "", message = "";
	
	private bool register = false;

	private void OnGUI ()
	{

		if (message != "")
			GUILayout.Box (message);
		
		if (register) {
			GUILayout.Label ("Username");
			user = GUILayout.TextField (user);
			GUILayout.Label ("password");
			password = GUILayout.PasswordField (password, "*" [0]);
			GUILayout.Label ("Re-password");
			rePass = GUILayout.PasswordField (rePass, "*" [0]);
			
			GUILayout.BeginHorizontal ();
			
			if (GUILayout.Button ("Back"))
				register = false;
			
			if (GUILayout.Button ("Register")) {
				message = "Registrando usuario...";
				
				if (user == "" || password == "")
					message = "Debes rellenar todos los campos \n";
				else {
					if (password == rePass) {
						
						WWW w = DAOManager.instance.register (user, password);
						StartCoroutine (registerFunc (w));

					} else
						message = "The passwords are diferent.\n";
				}
			}
			
			GUILayout.EndHorizontal ();
		} else {
			GUILayout.Label ("User:");
			user = GUILayout.TextField (user);
			GUILayout.Label ("Password:");
			password = GUILayout.PasswordField (password, "*" [0]);
			
			GUILayout.BeginHorizontal ();
			
			if (GUILayout.Button ("Login")) {
				message = "Trying login...";
				
				if (user == "" || password == "")
					message = "You must insert all fields.\n";
				else {

					WWW w = DAOManager.instance.login (user, password);
					StartCoroutine (loginFunc (w));

				}
			}
			
			if (GUILayout.Button ("Register"))
				register = true;
			
			GUILayout.EndHorizontal ();
		}
	}

	IEnumerator loginFunc (WWW w)
	{
		yield return w;

		string[] s = w.text.Split ('#');

		print (s[0]);

		if (w.error == null) {
			if (s [0].Equals ("1")) {
				message = "You logged succesfully!";
				SceneManager.LoadScene ("Menus");
			} else {
				message = s[1];
			}
		} else {
			message = "ERROR: " + w.error + "\n";
		}
	}

	IEnumerator registerFunc (WWW w)
	{
		yield return w;
		if (w.error == null) {
			message = w.text;
		} else {
			message = "ERROR: " + w.error + "\n";
		}
	}
}

