﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;
using System;
using System.Linq;

public class ScoresTest : MonoBehaviour
{

	public static string user = "";
	private string message = "", min = "", max = "", idGame = "", idUser = "", score = "";

	// 0 select All Scores from 1 gameScreen, 1 select Score from 1 gamesCreen by IdUser, 2  insert score.
	private int op = 0;

	private void OnGUI ()
	{
		// Show all scores from 1 gameScreen
		if (op == 0) {
			if (message != "")
				GUILayout.Box (message);

			GUILayout.Label ("Min results:");
			min = GUILayout.TextField (min);
			GUILayout.Label ("Max results:");
			max = GUILayout.TextField (max);
			GUILayout.Label ("ID Game:");
			idGame = GUILayout.TextField (idGame);


			GUILayout.BeginHorizontal ();

			if (GUILayout.Button ("Query")) {
			
				int minInt = 0;
				int maxInt = 0;
				int idGameInt = 0;

				// Expresions regulars que eliminen tot ho que no son nombres.
				min = Regex.Replace (min, "[^0-9]", "");
				max = Regex.Replace (max, "[^0-9]", "");
				idGame = Regex.Replace (idGame, "[^0-9]", "");

				if (int.TryParse (min, out minInt) && int.TryParse (max, out maxInt) && int.TryParse (idGame, out idGameInt)) {
					print ("min: " + minInt);
					print ("max: " + maxInt);
					print ("idGame: " + idGameInt);

					if (minInt < 0)
						min = "0";
					if (maxInt < 0)
						max = "0";

					WWW w = DAOManager.instance.getAllScoresByGameScreen (minInt, maxInt, idGameInt);
					StartCoroutine (processWWW (w));

				} else
					message = "Debes rellenar todos los campos \n";
			
			}

			if (GUILayout.Button ("FindScoreByIdUser")) {
				op = 1;
			}

			if (GUILayout.Button ("Insert Score")) {
				op = 2;
			}

			GUILayout.EndHorizontal ();


		}
		// Show all scores from 1 gameScreen by idUser
		else if (op == 1) {
			if (message != "")
				GUILayout.Box (message);

			GUILayout.Label ("Min results:");
			min = GUILayout.TextField (min);
			GUILayout.Label ("Max results:");
			max = GUILayout.TextField (max);
			GUILayout.Label ("ID Game:");
			idGame = GUILayout.TextField (idGame);
			GUILayout.Label ("ID User:");
			idUser = GUILayout.TextField (idUser);

			GUILayout.BeginHorizontal ();

			if (GUILayout.Button ("Query")) {

				int minInt = 0;
				int maxInt = 0;
				int idGameInt = 0;
				int idUserInt = 0;

				// Expresions regulars que eliminen tot ho que no son nombres.
				min = Regex.Replace (min, "[^0-9]", "");
				max = Regex.Replace (max, "[^0-9]", "");
				idGame = Regex.Replace (idGame, "[^0-9]", "");
				idUser = Regex.Replace (idUser, "[^0-9]", "");

				if (int.TryParse (min, out minInt) && int.TryParse (max, out maxInt) && int.TryParse (idGame, out idGameInt) && int.TryParse (idUser, out idUserInt)) {

					if (minInt < 0)
						min = "0";
					if (maxInt < 0)
						max = "0";

//					WWW w = DAOManager.instance.getScoresByGameScreenByIdUser (minInt, maxInt, idGameInt, idUserInt);
//					StartCoroutine (processWWW (w));

				} else
					message = "Debes rellenar todos los campos \n";

			}

			if (GUILayout.Button ("FindScore")) {
				op = 0;
			}

			if (GUILayout.Button ("Insert Score")) {
				op = 2;
			}

			GUILayout.EndHorizontal ();
		}
		// Insert Score
		else if (op == 2) {

			if (message != "")
				GUILayout.Box (message);

			GUILayout.Label ("Score:");
			score = GUILayout.TextField (score);
			GUILayout.Label ("idGame:");
			idGame = GUILayout.TextField (idGame);
			GUILayout.Label ("idUser:");
			idUser = GUILayout.TextField (idUser);

			GUILayout.BeginHorizontal ();

			if (GUILayout.Button ("Insert")) {

				int scoreInt = 0;
				int idGameInt = 0;
				int idUserInt = 0;

				// Expresions regulars que eliminen tot ho que no son nombres.
				score = Regex.Replace (idGame, "[^0-9]", "");
				idGame = Regex.Replace (idGame, "[^0-9]", "");
				idUser = Regex.Replace (idGame, "[^0-9]", "");

				if (int.TryParse (score, out scoreInt) && int.TryParse (idGame, out idGameInt) && int.TryParse (idUser, out idUserInt)) {
					print ("idGame: " + idGameInt);
					print ("idGame: " + idUserInt);

//					WWW w = DAOManager.instance.insertScore (scoreInt, idGameInt, idUserInt);
//					StartCoroutine (processWWW (w));

				} else
					message = "Debes rellenar todos los campos \n";
			}

			if (GUILayout.Button ("FindScore")) {
				op = 0;
			}

			if (GUILayout.Button ("FindScoreById")) {
				op = 1;
			}

			GUILayout.EndHorizontal ();

		}
	}

	IEnumerator processWWW (WWW w)
	{
		yield return w;
		if (w.error == null) {
			message = w.text;
		} else {
			message = "ERROR: " + w.error + "\n";
		}
	}
}
