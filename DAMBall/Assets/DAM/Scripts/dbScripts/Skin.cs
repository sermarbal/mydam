﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skin {

	private float price;
	private string name;

	public Skin(string name, float price){
		this.name = name;
		this.price = price;
	}

}
