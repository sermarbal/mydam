﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// Allow to select the level that want to play the current player.
/// </summary>
public class MenuLevels: MonoBehaviour
{

	// Canvas
	public Canvas menuMode;

	// Buttons
	public Button[] btnLevels;
	public Button btnReturn;

	// AudioClips
	public AudioClip btnClickEffect;
	public AudioClip btnClickReturnEffect;

	/// <summary>
	/// Initialize the Levels Menu.
	/// </summary>
	void Start ()
	{

		// Add listeners to buttons.
		for (int i = 0; i < btnLevels.Length; i++) {
			int aux = i + 1;
			btnLevels [i].onClick.AddListener (() => onClickLevelButton ("Level " + (aux)));
		}
		btnReturn.onClick.AddListener (() => onClickReturn ());

	}

	/// <summary>
	/// Loads the choosed Level.
	/// </summary>
	/// <param name="level">Level name to load.</param>
	private void onClickLevelButton (String level)
	{
		// Play the sound.
		SoundManager.instance.PlaySingle (btnClickEffect);
		// Show Message loading...
		MessageManager.instance.showMessage("Loading...");
		// Load the level.
		SceneManager.LoadScene (level);
		MessageManager.instance.closeMessage ();
			
	}

	private void onClickReturn ()
	{
		// Play button effect.
		SoundManager.instance.PlaySingle (btnClickReturnEffect);
		// Set Game Mode to none
		PlayerManager.instance.gameMode=0;
		// Disable Levels Canvas
		this.GetComponent<Canvas>().enabled = false;
		// Enable Mode Menu.
		menuMode.enabled = true;
	}

	public void enableButtonsFree(){
		for (int i = 0; i < btnLevels.Length; i++)
			btnLevels [i].interactable = true;
	}

	public void disableButtonsHistory(int level){
		for (int i = level; i < btnLevels.Length; i++)
			btnLevels [i].interactable = false;
	}

}
