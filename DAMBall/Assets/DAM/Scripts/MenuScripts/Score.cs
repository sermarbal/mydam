﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class used for represent a score.
/// </summary>
public class Score {

	public string score;
	public string username;

	public Score(string username, string score){
		
		this.score = score;
		this.username = username;

	}



}
