﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Allow to see the current scores in the server.
/// </summary>
public class MenuScores : MonoBehaviour
{
	// Scores parent in mask
	public GameObject scoresParent;

	// Score prefab
	public GameObject scorePrefab;

	// Canvas main
	public Canvas menuMain;

	// Button
	public Button btnReturn;

	// Effect sounds
	public AudioClip btnClickReturnEffect;

	// variables needed reload the scores correctly
	private float cont = 0;
	private float intervalReloadScores = 10;

	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start ()
	{
		//Canvas
		menuMain = menuMain.GetComponent<Canvas> ();

		// Add the listener to return button
		btnReturn.onClick.AddListener (() => onClickReturn ());

		WWW w = DAOManager.instance.getGlobalScores (0, 50);
		StartCoroutine (processWWW (w));

	}

	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update(){

		cont += Time.deltaTime;
		// if the counter is ready we reload the scores.
		if (cont >= intervalReloadScores) {

			// Removes all the current scores
			foreach (Transform child in scoresParent.transform) {
				GameObject.Destroy(child.gameObject);
			}
			// Cet and put the new 50 scores
			WWW w = DAOManager.instance.getGlobalScores (0, 50);
			StartCoroutine (processWWW (w));

			// Reinitialize the counter
			cont = 0;
		}
	}

	private void insertScores (List<Score> list)
	{

		// For every Score in the list...
		for (int i = 0; i < list.Count; i++) {
			// Initialize the Score prefab
			GameObject tmpObject = Instantiate (scorePrefab);
			// Get the script that inserts the information in the prefab.
			tmpObject.GetComponentInChildren<ScoreScript> ().setScore ((i + 1).ToString (), list [i].username, list [i].score);
			// Set the parent object
			tmpObject.transform.SetParent (scoresParent.transform);
			// Scale the prefab to the correct size
			tmpObject.GetComponent<RectTransform> ().localScale = new Vector3 (1, 1, 1);
			// Moves the prefab to the correct location
			tmpObject.GetComponent<RectTransform> ().localPosition = new Vector3 (1, 1, 0);

		}

	}

	/// <summary>
	/// returns to previous canvas
	/// </summary>
	private void onClickReturn ()
	{
		SoundManager.instance.PlaySingle (btnClickReturnEffect);
		menuMain.enabled = true;
		this.GetComponent<Canvas> ().enabled = false;
	}

	/// <summary>
	/// Processes the WWW request for get the scores.
	/// </summary>
	/// <returns>The scores</returns>
	/// <param name="w">WWW class with the request to the server for get the scores</param>
	private IEnumerator processWWW (WWW w)
	{
		yield return w;
		if (w.error == null) {
			
			string[] values = w.text.Split ('#');

			if (int.Parse (values [0]) == 1) {

				List<Score> list = new List<Score> ();

				for (int i = 1; i < values.Length; i++) {
					string[] parameters = values [i].Split ('|');
					list.Add (new Score (parameters [0], parameters [1]));
				}
				insertScores (list);
			} else
				print ("Error: "+ values[1]);
			

		} else {
			if (Application.internetReachability == NetworkReachability.NotReachable) {
				MessageManager.instance.showMessage (3, "Error. Check internet connection!");
			} else {
				// if an error ocurred we show the connection error
				MessageManager.instance.showMessage (3, w.error);
			}
		}
	}

}
