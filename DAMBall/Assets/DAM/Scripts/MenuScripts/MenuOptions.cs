﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Allow to change the music and effects volume.
/// </summary>
public class MenuOptions : MonoBehaviour
{
	// Canvas related with this menu
	public Canvas menuMain;

	// Sound Sliders 
	public Slider sliderMusic;
	public Slider sliderEffects;

	// Return to back menu.
	public Button btnReturn;

	// Audioclips with the sound effects of the buttons.
	public AudioClip btnClickEffect;
	public AudioClip btnClickReturnEffect;

	// Count for play the effect managing the effects scrollbar.
	public float countEffectSliderBar = 0;

	/// <summary>
	/// Initialize the Options menu.
	/// </summary>
	void Start ()
	{

		//Slider
		sliderMusic = sliderMusic.GetComponent<Slider> ();

		//Slider Effects
		sliderEffects = sliderEffects.GetComponent<Slider> ();

		// Return button add listener
		btnReturn.onClick.AddListener (() => onClickReturn ());
	}

	/// <summary>
	/// Update Options Menu
	/// </summary>
	void Update(){
		countEffectSliderBar += Time.deltaTime;
	}

	/// <summary>
	/// Update the Options GUI
	/// </summary>
	void OnGUI ()
	{
		// Update the music value.
		SoundManager.instance.musicSource.volume = sliderMusic.value;
		// Update the effects value.
		SoundManager.instance.efxSource.volume = sliderEffects.value;

		// Add the listener for the test volume effect while the player moves the slidereffect bar.
		sliderEffects.onValueChanged.AddListener (delegate {
			checkValueEffectSliderBar ();
		});

	}

	/// <summary>
	/// Raw image with a button that returns previous menu. (Main Menu)
	/// </summary>
	private void onClickReturn ()
	{
		// Play the sound.
		SoundManager.instance.PlaySingle (btnClickReturnEffect);
		// Enable target menu.
		menuMain.enabled = true;
		// Disable this menu.
		this.GetComponent<Canvas>().enabled = false;
	}

	/// <summary>
	/// Play the sound related to effects slider every 2 seconds.
	/// </summary>
	private void checkValueEffectSliderBar ()
	{
		// If count related to effect bar
		if (countEffectSliderBar > 2) {
			// Play sound.
			SoundManager.instance.PlaySingle (btnClickEffect);
			// Return count to 0.
			countEffectSliderBar = 0;
		}
	}

}
