﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Script used in the score prefab for insert the score in the UI.
/// </summary>
public class ScoreScript : MonoBehaviour{

	// gameobjects that contains the visual UI Score.
	public GameObject rank;
	public GameObject username;
	public GameObject score;

	/// <summary>
	/// Sets the score in the prefabs elements.
	/// </summary>
	/// <param name="rank">Rank of the player in the prefab.</param>
	/// <param name="username">Username of the player in the prefab.</param>
	/// <param name="score">Score of the player in the prefab.</param>
	public void setScore(string rank, string username, string score){

		this.rank.GetComponent<Text>().text = rank;
		this.score.GetComponent<Text>().text = score;
		this.username.GetComponent<Text>().text = username;

	}



}
