﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// Show the exit dialog.
/// </summary>
public class ExitDialog : MonoBehaviour {

	// Canvas
	public Canvas menuLoginRegister;

	// Buttons
    public Button btnYes;
    public Button btnNo;

	// AudioClips
	public AudioClip continuePlayingSound;
	public AudioClip stopPlayingSound;

	/// <summary>
	/// Initialize the Exit Dialog.
	/// </summary>
    void Start () {

		// Add listeners to buttons.
		btnYes.onClick.AddListener(() => onClickYes());
		btnNo.onClick.AddListener(() => onClickNo());

	}

	/// <summary>
	/// Click Yes
	/// </summary>
	private void onClickYes()
	{
		// Play sound
		SoundManager.instance.PlaySingle (stopPlayingSound);
		// Exit application
		Application.Quit();
	}

	/// <summary>
	/// Click No
	/// </summary>
    private void onClickNo()
    {
		// Play sound
		SoundManager.instance.PlaySingle (continuePlayingSound);
		// Change canvas
		menuLoginRegister.enabled = true;
		this.GetComponent<Canvas>().enabled = false;
    }


}
