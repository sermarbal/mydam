﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/// <summary>
/// Allow register and login to play the game.
/// </summary>
public class MenuLoginRegister : MonoBehaviour
{

	// Canvas
	public Canvas menuMain;
	public Canvas menuExit;


	// Buttons Login/Register
	public Button btnLogin;
	public Button btnLoginAnonymous;
	public Button btnRegister;
	public Button btnExit;

	// Fields Login
	public GameObject loginUsername;
	public GameObject loginPassword;
	private string loginUsernameString;
	private string loginPasswordString;

	// Fields Register
	public GameObject registerUsername;
	public GameObject registerPassword;
	public GameObject registerRePassword;
	private string registerUsernameString;
	private string registerPasswordString;
	private string registerRePasswordString;

	// Audio effect clic on button
	public AudioClip btnAudio;

	/// <summary>
	/// Initialize the Login/Register Menu adding the buttons and the listeners to these.
	/// </summary>
	void Start ()
	{

		// Add listeners to buttons.
		btnLogin.onClick.AddListener (login);
		btnLoginAnonymous.onClick.AddListener (loginAnonymous);
		btnRegister.onClick.AddListener (register);
		btnExit.onClick.AddListener (exit);

	}

	/// <summary>
	/// Relate the strings witht he textfields respectively.
	/// </summary>
	void OnGUI ()
	{

		// Update Login vars
		loginUsernameString = loginUsername.GetComponent<InputField> ().text;
		loginPasswordString = loginPassword.GetComponent<InputField> ().text;

		// Update Register vars
		registerUsernameString = registerUsername.GetComponent<InputField> ().text;
		registerPasswordString = registerPassword.GetComponent<InputField> ().text;
		registerRePasswordString = registerRePassword.GetComponent<InputField> ().text;

		// Enter
		if (Input.GetKeyDown (KeyCode.Return) || Input.GetKeyDown (KeyCode.KeypadEnter)) {
			if (loginUsername.GetComponent<InputField> ().isFocused || loginPassword.GetComponent<InputField> ().isFocused) {
				login ();
			} else if (registerUsername.GetComponent<InputField> ().isFocused || registerPassword.GetComponent<InputField> ().isFocused || registerRePassword.GetComponent<InputField> ().isFocused) {
				register ();
			}
		}

	}

	/// <summary>
	/// Tabulation code for LoginRegister Inputfields
	/// </summary>
	void Update(){

		// Tabulation in LoginRegister
		if(Input.GetKeyDown (KeyCode.Tab) && Input.GetKey (KeyCode.LeftShift)){
			if(EventSystem.current.currentSelectedGameObject==null)
				loginUsername.GetComponent<InputField> ().Select ();
			else if (loginUsername.GetComponent<InputField> ().isFocused) {
				registerRePassword.GetComponent<InputField> ().Select ();
			} else if (loginPassword.GetComponent<InputField> ().isFocused) {
				loginUsername.GetComponent<InputField> ().Select ();
			} else if (registerUsername.GetComponent<InputField> ().isFocused) {
				loginPassword.GetComponent<InputField> ().Select ();
			} else if (registerPassword.GetComponent<InputField> ().isFocused) {
				registerUsername.GetComponent<InputField> ().Select ();
			} else if (registerRePassword.GetComponent<InputField> ().isFocused) {
				registerPassword.GetComponent<InputField> ().Select ();
			}
		} else if (Input.GetKeyDown (KeyCode.Tab)) {
			if(EventSystem.current.currentSelectedGameObject==null)
				loginUsername.GetComponent<InputField> ().Select ();
			else if (loginUsername.GetComponent<InputField> ().isFocused) {
				loginPassword.GetComponent<InputField> ().Select ();
			} else if (loginPassword.GetComponent<InputField> ().isFocused) {
				registerUsername.GetComponent<InputField> ().Select ();
			} else if (registerUsername.GetComponent<InputField> ().isFocused) {
				registerPassword.GetComponent<InputField> ().Select ();
			} else if (registerPassword.GetComponent<InputField> ().isFocused) {
				registerRePassword.GetComponent<InputField> ().Select ();
			} else if (registerRePassword.GetComponent<InputField> ().isFocused) {
				loginUsername.GetComponent<InputField> ().Select ();
			}
		}

	}

	/// <summary>
	/// Login the user in the login form.
	/// </summary>
	private void login ()
	{
		// Plays the audio clic.
		SoundManager.instance.PlaySingle (btnAudio);

		// Control if textfields for logging are empty
		if (loginUsernameString.Equals ("")) {
			MessageManager.instance.showMessage (3, "You must put an username for logging!");
		} else if (loginPasswordString.Equals ("")) {
			MessageManager.instance.showMessage (3, "You must put a password for logging!");
		} else {
			// If the textfields are not empty process to make the login.
			WWW w = DAOManager.instance.login (loginUsernameString, loginPasswordString);
			// Process the thread to login
			StartCoroutine (loginThread (w));
		}

	}

	/// <summary>
	/// Login the Anonymous predefined player.
	/// </summary>
	private void loginAnonymous ()
	{
		// Plays the audio clic.
		SoundManager.instance.PlaySingle (btnAudio);

		// Make the anonymous login.
		WWW w = DAOManager.instance.login ("Anonymous", "1234");
		// Process the thread to login
		StartCoroutine (loginThreadAnonymous (w));

	}

	/// <summary>
	/// Register the player in the register form.
	/// </summary>
	private void register ()
	{
		// Plays the audio clic.
		SoundManager.instance.PlaySingle (btnAudio);

		// Control if textfields for register are empty
		if (registerUsernameString.Equals ("")) {
			MessageManager.instance.showMessage (3, "You must put an username for register!");
		} else if (registerPasswordString.Equals ("")) {
			MessageManager.instance.showMessage (3, "You must put a password for register!");
		} else if (registerRePasswordString.Equals ("")) {
			MessageManager.instance.showMessage (3, "You must put a repassword for register!");
		} else if (registerPasswordString.Equals (registerRePasswordString)) {
			// If the textfields are not empty process to make the register.
			WWW w = DAOManager.instance.register (registerUsernameString, registerPasswordString);
			// Process the thread to register
			StartCoroutine (registerThread (w));
		} else
			MessageManager.instance.showMessage (3, "Your passwords are different!");
	}

	/// <summary>
	/// Asynchronous Thread where the applitacion try to login the user of the login form.
	/// </summary>
	/// <returns>IEnumerator for StartCoroutine works.</returns>
	/// <param name="w">WWW class with the route to the server php file.</param>
	private IEnumerator loginThread (WWW w)
	{
		// Show message logging... while we wait the response of the server.
		MessageManager.instance.showMessage ("Logging...");
		// if we receibe the response it continues after this point.
		yield return w;
		// We have the response so the message "Logging" is close here.
		MessageManager.instance.closeMessage ();

		// Control if we have an error in the response of the server.
		if (w.error == null) {
			// Split the response for get the data.
			string[] arrayResponse = w.text.Split ('#');
			// If the array[0] is 0 the response is not ok and is ok if it is 1
			if (arrayResponse [0].Equals ("1")) {

				// If is OK we must split another time the second parameter "array[1]"
				string[] values = arrayResponse [1].Split ('|');
				// We put the player information to PlayerManager.
				PlayerManager.instance.id = int.Parse (values [0]);
				PlayerManager.instance.username = values [1];

				// Welcome [name] message
				MessageManager.instance.showMessage (3, "Welcome " + PlayerManager.instance.username);

				// Activate the main menu
				menuMain.enabled = true;
				// disable the login register menu
				this.GetComponent<Canvas> ().enabled = false;

				// If array[0] is 0 the response is not ok and we show the response
			} else if (arrayResponse [0].Equals ("0")) {
				MessageManager.instance.showMessage (3, arrayResponse [1]);
			}
		} else {
			if (Application.internetReachability == NetworkReachability.NotReachable) {
				MessageManager.instance.showMessage (3, "Error. Check internet connection!");
			} else {
				// if an error ocurred we show the connection error
				MessageManager.instance.showMessage (3, w.error);
			}
		}
		resetFields ();
	}

	/// <summary>
	/// Asynchronous Thread where the applitacion try to login the Anonymous player.
	/// </summary>
	/// <returns>IEnumerator for StartCoroutine works.</returns>
	/// <param name="w">WWW class with the route to the server php file.</param>
	private IEnumerator loginThreadAnonymous (WWW w)
	{
		
		// Show message logging... while we wait the response of the server.
		MessageManager.instance.showMessage ("Logging...");
		// if we receibe the response it continues after this point.
		yield return w;
		// We have the response so the message "Logging" is close here.
		MessageManager.instance.closeMessage ();

		// Control if we have an error in the response of the server.
		if (w.error == null) {
			// Split the response for get the data.
			string[] arrayResponse = w.text.Split ('#');
			// If the array[0] is 0 the response is not ok and is ok if it is 1
			if (arrayResponse [0].Equals ("1")) {
				
				// If is OK we must split another time the second parameter "array[1]"
				string[] values = arrayResponse [1].Split ('|');
				// We put the player information to PlayerManager.
				PlayerManager.instance.id = int.Parse (values [0]);
				PlayerManager.instance.username = values [1];

				// Welcome [name] message
				MessageManager.instance.showMessage (1, "Welcome " + PlayerManager.instance.username);

				// Activate the main menu
				menuMain.enabled = true;
				// disable the login register menu
				this.GetComponent<Canvas> ().enabled = false;

				// If array[0] is 0 the response is not ok and we show the response
			} else if (arrayResponse [0].Equals ("0")) {
				MessageManager.instance.showMessage (3, arrayResponse [1]);
			}
		} else {
			// We can know if the player is connected internet using this method
			if (Application.internetReachability == NetworkReachability.NotReachable) {
				MessageManager.instance.showMessage (3, "Error. Check internet connection!");
			} else {
				// if an error ocurred we show the connection error
				MessageManager.instance.showMessage (3, w.error);
			}
		}
		resetFields ();
	}

	/// <summary>
	/// Asynchronous Thread where the applitacion try to register the player from register form.
	/// </summary>
	/// <returns>IEnumerator for StartCoroutine works.</returns>
	/// <param name="w">WWW class with the route to the server php file.</param>
	private IEnumerator registerThread (WWW w)
	{
		// Show message logging... while we wait the response of the server.
		MessageManager.instance.showMessage ("Recording...");
		// if we receibe the response it continues after this point.
		yield return w;
		// We have the response so the message "Recording..." is close here.
		MessageManager.instance.closeMessage ();

		// Control if we have an error in the response of the server.
		if (w.error == null) {
			// Split the response for get the data.
			string[] arrayResponse = w.text.Split ('#');
			// If the array[0] is 0 the response is not ok and is ok if it is 1
			if (arrayResponse [0].Equals ("1")) {

				// Show OK Message
				MessageManager.instance.showMessage (3, arrayResponse [1]);

				// If array[0] is 0 the response is not ok and we show the response
			} else if (arrayResponse [0].Equals ("0")) {
				MessageManager.instance.showMessage (3, arrayResponse [1]);
			}
		} else {
			if (Application.internetReachability == NetworkReachability.NotReachable) {
				MessageManager.instance.showMessage (3, "Error. Check internet connection!");
			} else {
				// if an error ocurred we show the connection error
				MessageManager.instance.showMessage (3, w.error);
			}
		}
		resetFields ();
	}

	private void resetFields(){
		// Update Login vars
		loginUsername.GetComponent<InputField> ().text = "";
		loginPassword.GetComponent<InputField> ().text = "";

		// Update Register vars
		registerUsername.GetComponent<InputField> ().text = "";
		registerPassword.GetComponent<InputField> ().text = "";
		registerRePassword.GetComponent<InputField> ().text = "";
	}

	private void exit ()
	{

		// Plays the audio clic.
		SoundManager.instance.PlaySingle (btnAudio);

		// Disable the login register canvas.
		this.GetComponent<Canvas> ().enabled = false;

		menuExit.enabled = true;

	}

}
