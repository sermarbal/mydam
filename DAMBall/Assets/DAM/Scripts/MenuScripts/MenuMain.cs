﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// Main menu where we can play, see the scores, go to options and loggout.
/// </summary>
public class MenuMain : MonoBehaviour {

    // Canvas related with this menu
    public Canvas menuPlay;
    public Canvas menuOptions;
    public Canvas menuScore;
    public Canvas menuLoginRegister;

	// Raw Image Mute Button
	public RawImage rawImageMute;

	// Mute/Demute Textures
	public Texture mute;
	public Texture notMute;

    // Buttons
    public Button btnPlay;
    public Button btnOptions;
    public Button btnScore;
    public Button btnLoggout;

	// Audioclib with the button sound reference.
	public AudioClip btnClickEffect;
    public Button btnMute;

	/// <summary>
	/// Initialize the menu manager
	/// </summary>
    void Start()
    {

		// Add listeners
		btnPlay.onClick.AddListener(() => onClickPlay());
		btnOptions.onClick.AddListener(() => onClickOptions());
		btnScore.onClick.AddListener(() => onClickScore());
		btnLoggout.onClick.AddListener(() => onClickLoggout());
		btnMute.onClick.AddListener(() => onClickMuteSound());

    }

    /// <summary>
    /// Opens the difficulty menu.
    /// </summary>
    public void onClickPlay()
    {
		// Play the button sound.
		SoundManager.instance.PlaySingle (btnClickEffect);
		// desactivate this canvas.
		this.GetComponent<Canvas>().enabled = false;
		// Enable the difficulty menu.
        menuPlay.enabled = true;

    }

	/// <summary>
	/// Opens the Options Menu
	/// </summary>
    private void onClickOptions()
    {
		// Play the button sound.
		SoundManager.instance.PlaySingle (btnClickEffect);
		// desactivate this canvas.
		this.GetComponent<Canvas>().enabled = false;
		// Enable menu options
        menuOptions.enabled = true;
    }

	/// <summary>
	/// Opens the Score screen
	/// </summary>
    private void onClickScore()
    {
		// Play the button sound.
		SoundManager.instance.PlaySingle (btnClickEffect);
		// desactivate this canvas.
		this.GetComponent<Canvas>().enabled = false;
		// Enable menu score canvas.
        menuScore.enabled = true;
    }

	/// <summary>
	/// Deletes the current user information and redirect to Login Register menu.
	/// </summary>
    private void onClickLoggout()
    {
		// Play the button sound.
		SoundManager.instance.PlaySingle (btnClickEffect);
		// desactivate this canvas.
		this.GetComponent<Canvas>().enabled = false;

		// Deletes the current player information.
		PlayerManager.instance.id = 0;
		PlayerManager.instance.username = null;

		// Enable menu LoginRegister.
		menuLoginRegister.enabled = true;
    }

    /// <summary>
    /// Mute/Demute the music and effects.
    /// </summary>
	private void onClickMuteSound()
    {
		// Play the button sound.
		SoundManager.instance.PlaySingle (btnClickEffect);

		// Mute/Demute the music and effects changin the mute raw image respectively.
		if(SoundManager.instance.musicSource.mute || SoundManager.instance.efxSource.mute)
        {
			// Mute Music.
			SoundManager.instance.musicSource.mute = false;
			// Mute Effects.
			SoundManager.instance.efxSource.mute = false;
			// Change RawImage.
			rawImageMute.texture = mute;
        }else
        {
			// Mute Music.
			SoundManager.instance.musicSource.mute = true;
			// Mute Effects.
			SoundManager.instance.efxSource.mute = true;
			// Change RawImage.
			rawImageMute.texture = notMute;
        }
         
    }
}
