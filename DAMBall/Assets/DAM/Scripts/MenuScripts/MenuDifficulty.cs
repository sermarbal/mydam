﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// Allow to select the difficulty that the player want to play.
/// </summary>
public class MenuDifficulty : MonoBehaviour {

	// Raw Image general background
    public RawImage rawImageBackground;

	// Textures for difficulties
	public Texture defaultBackground;
    public Texture easyBackground;
    public Texture normalBackground;
    public Texture hardBackground;

	// Canvas with the different
    public Canvas menuMain;
    public Canvas menuMode;
    
	// Buttons
    public Button easy;
    public Button normal;
    public Button hard;
    public Button returnButton;

	// AudioClips
	public AudioClip btnClicEffect;
	public AudioClip btnClicReturnEffect;

    /// <summary>
    /// Initialization menu difficulty
    /// </summary>
    void Start()
    {
		// Assign listeners to the buttons.
		easy.onClick.AddListener(() => onClickEasyMode());
		normal.onClick.AddListener(() => onClickNormalMode());
		hard.onClick.AddListener(() => onClickHardMode());
		returnButton.onClick.AddListener(() => onClickReturnButton());
    }

	/// <summary>
	/// Open Mode menu choosing easy difficulty.
	/// </summary>
	private void onClickEasyMode()
	{
		// Play positive sound button.
		SoundManager.instance.PlaySingle (btnClicEffect);
		// Change background to easy background.
		rawImageBackground.texture = easyBackground;
		// Assign Difficulty to PlayerManager
		PlayerManager.instance.difficulty = 1;
		// Disable Difficulty Menu.
		this.GetComponent<Canvas>().enabled = false;
		// Enable Game Mode Menu.
		menuMode.enabled = true;


	}

	/// <summary>
	/// Open Mode Menu choosing normal difficulty.
	/// </summary>
    private void onClickNormalMode()
    {
		// Play positive sound button.
		SoundManager.instance.PlaySingle (btnClicEffect);
		// Assign difficulty to PlayerManager
		PlayerManager.instance.difficulty = 2;
		// Change background to normal background.
		rawImageBackground.texture = normalBackground;
		// Disable Difficulty Menu.
		this.GetComponent<Canvas>().enabled = false;
		// Enable Mode Menu.
        menuMode.enabled = true;

    }

	/// <summary>
	/// Open Mode Menu choosing hard difficulty.
	/// </summary>
	private void onClickHardMode()
	{
		// Play positive sound button.
		SoundManager.instance.PlaySingle (btnClicEffect);
		// Assign difficulty to PlayerManager
		PlayerManager.instance.difficulty = 3;
		// Change background to Hard background.
		rawImageBackground.texture = hardBackground;
		// Disable Difficulty Menu.
		this.GetComponent<Canvas>().enabled = false;
		// Enable Mode Menu.
		menuMode.enabled = true;
	}

	/// <summary>
	/// Return to Main Menu.
	/// </summary>
	private void onClickReturnButton()
	{
		// Play audio return button.
		SoundManager.instance.PlaySingle (btnClicReturnEffect);
		// Assign difficulty to PlayerManager 0 (No difficulty select)
		PlayerManager.instance.difficulty = 0;
		// Disable difficulty menu.
		this.GetComponent<Canvas>().enabled = false;
		// Enable Main Menu.
		menuMain.enabled = true;
	}

}
