﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Allow to open the Menus scene depending the data in PlayerManager and contains methods for menu Scene.
/// </summary>
public class LoadMenus : MonoBehaviour {

	// Other canvas
	public Canvas menuLoginRegister;
	public Canvas menuMain;
	public Canvas menuDifficulty;
	public Canvas menuMode;
	public Canvas menuLevelList;
	public Canvas menuOptions;
	public Canvas menuScores;
	public Canvas menuExit;
	public Canvas menuSkins;

	// Raw Image general background
	public RawImage rawImageBackground;

	// Textures for difficulties
	public Texture defaultBackground;
	public Texture easyBackground;
	public Texture normalBackground;
	public Texture hardBackground;

	// Use this for initialization
	void Start () {

		if(PlayerManager.instance.id > 0 && PlayerManager.instance.difficulty > 0 && PlayerManager.instance.gameMode > 0) {
			switch (PlayerManager.instance.difficulty) {
			case 1:
				rawImageBackground.texture = easyBackground;
				break;
			case 2:
				rawImageBackground.texture = normalBackground;
				break;
			case 3:
				rawImageBackground.texture = hardBackground;
				break;
			}
			switch (PlayerManager.instance.gameMode) {
			case 1:
				updateHistoryLevels ();
				break;
			case 2:
				updateFreeLevels ();
				break;
			}
			// Disable all Menus less 
			menuLoginRegister.enabled = false;
			menuMain.enabled = false;
			menuDifficulty.enabled = false;
			menuMode.enabled = false;
			menuOptions.enabled = false;
			menuScores.enabled = false;
			menuExit.enabled = false;

		} else if (PlayerManager.instance.id == 0) {
			// Disable all the other menus.
			menuLoginRegister.enabled=true;
			menuMain.enabled = false;
			menuDifficulty.enabled = false;
			menuMode.enabled = false;
			menuLevelList.enabled = false;
			menuOptions.enabled = false;
			menuScores.enabled = false;
			menuExit.enabled = false;
		}

	}

	/// <summary>
	/// Update the levels in free mode.
	/// </summary>
	public void updateFreeLevels(){
		// Find the menu Levels GameObject.
		GameObject menuLevels = GameObject.Find ("Menu Levels");
		// Unlocks the free levels mode. (all)
		menuLevels.GetComponent<MenuLevels>().enableButtonsFree();
	}

	/// <summary>
	/// Close the buttons to scenes that are not opened to the user.
	/// </summary>
	public void updateHistoryLevels(){
		// First takes the games completed by the user.
		WWW w = DAOManager.instance.getGamesDoneByIdUser (PlayerManager.instance.id);
		// Process the request to the server.
		StartCoroutine (disableLevels (w));
	}

	/// <summary>
	/// Make the request to server and close the not available levels.
	/// </summary>
	/// <returns>The levels.</returns>
	/// <param name="w">WWW class with the request to the server.</param>
	private IEnumerator disableLevels (WWW w)
	{
		// Show message logging... while we wait the response of the server.
//		MessageManager.instance.showMessage ("Loading...");
		// if we receibe the response it continues after this point.
		yield return w;
		// We have the response so the message "Logging" is close here.
//		MessageManager.instance.closeMessage ();
		// Control if we have an error in the response of the server.
		if (w.error == null) {
			// Split the response for get the data.
			string[] arrayResponse = w.text.Split ('#');
			// If the array[0] is 0 the response is not ok and is ok if it is 1
			if (arrayResponse [0].Equals ("1")) {

				// Find the menu Levels GameObject.
				GameObject menuLevels = GameObject.Find ("Menu Levels");

				// We sum one more because we want to unlock the next level.
				int currentLevel = int.Parse(arrayResponse [1])+1;

				// How the history games always are opened orderly I did a for.
				menuLevels.GetComponent<MenuLevels>().disableButtonsHistory(currentLevel);

				// If array[0] is 0 the response is not ok and we show the response
			} else if (arrayResponse [0].Equals ("0")) {
				MessageManager.instance.showMessage (3, arrayResponse [1]);
			}
		} else {
			if (Application.internetReachability == NetworkReachability.NotReachable) {
				MessageManager.instance.showMessage (3, "Error. Check internet connection!");
			} else {
				// if an error ocurred we show the connection error
				MessageManager.instance.showMessage (3, w.error);
			}
		}
	}

}
