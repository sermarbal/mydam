﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// Allow to select the game mode that want to play the current player.
/// </summary>
public class MenuGameMode : MonoBehaviour
{

	// RawImage general background.
	public RawImage rawImageBackground;

	// Texture
	public Texture defaultBackground;

	// Canvas
	public Canvas menuDifficulty;
	public Canvas menuLevels;

	// Buttons
	public Button btnHistoryMode;
	public Button btnFreeMode;
	public Button btnReturn;

	// Sound button effects.
	public AudioClip btnClickEffect;
	public AudioClip btnClickReturnEffect;

	/// <summary>
	/// Initialize the Game Mode Menu.
	/// </summary>
	void Start ()
	{

		// Add listeners to buttons.
		btnHistoryMode.onClick.AddListener (() => onClickHistory ());
		btnFreeMode.onClick.AddListener (() => onClickFree ());
		btnReturn.onClick.AddListener (() => onClickReturn ());

	}

	/// <summary>
	/// Choose free mode option.
	/// </summary>
	private void onClickFree ()
	{
		// Play positive sound button
		SoundManager.instance.PlaySingle (btnClickEffect);
		// Choose Game Mode 2 (Free)
		PlayerManager.instance.gameMode = 2;
		// Disable Game Mode Menu.
		this.GetComponent<Canvas> ().enabled = false;
		// Load Available levels for the user in free mode.
		GetComponentInParent<LoadMenus>().updateFreeLevels();
		// Enable Levels Menu.
		menuLevels.enabled = true;
	}

	/// <summary>
	/// Choose history mode option.
	/// </summary>
	private void onClickHistory ()
	{
		// Play positive sound button
		SoundManager.instance.PlaySingle (btnClickEffect);
		// Choose Game Mode 1 (History)
		PlayerManager.instance.gameMode = 1;
		// Disable Game Mode Menu.
		this.GetComponent<Canvas> ().enabled = false;
		// Load Available levels for the user.
		GetComponentInParent<LoadMenus>().updateHistoryLevels();
		// Enable Levels Menu.
		menuLevels.enabled = true;
	}

	/// <summary>
	/// Return back to difficulty menu.
	/// </summary>
	private void onClickReturn ()
	{
		// Play sound return button.
		SoundManager.instance.PlaySingle (btnClickReturnEffect);
		// Set background to default texture.
		rawImageBackground.texture = defaultBackground;
		// Set Difficulty to none.
		PlayerManager.instance.difficulty = 0;
		// Disable Game Mode Menu.
		this.GetComponent<Canvas> ().enabled = false;
		// Enables Difficulty Menu.
		menuDifficulty.enabled = true;
	}

}
