﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;
using System.Security.Cryptography;
using System.IO;
using System.Text;

/// <summary>
/// Manages the connections and creates the class WWW for makes queries to the server.
/// </summary>
public class DAOManager: MonoBehaviour
{

	public static DAOManager instance = null;

	/// <summary>
	/// Initialize DAOManager Class
	/// </summary>
	void Awake ()
	{
		//Check if there is already an instance of DAOManager
		if (instance == null)
			//if not, set it to this.
			instance = this;
		//If instance already exists:
		else if (instance != this)
			//Destroy this, this enforces our singleton pattern so there can only be one instance of DAOManager.
			Destroy (gameObject);

		//Set DAOManager to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
		DontDestroyOnLoad (gameObject);
	}

	/// <summary>
	/// Build a WWW class to login an existent player.
	/// </summary>
	/// <returns>WWW Class to process after in a Coroutine method for get the data.</returns>
	/// <param name="user">The current user's id.</param>
	/// <param name="password">The password to login this user.</param>
	public WWW login (string user, string password)
	{

		WWWForm form = new WWWForm ();

		form.AddField ("user", user);
		form.AddField ("password", password);

		WWW w = new WWW ("http://labs.iam.cat/~a14sermarbal/DAM/login.php", form);
		// WWW w = new WWW ("http://192.168.1.133/DAM/login.php", form);

		return w;
	}

	/// <summary>
	/// Build the class WWW to register a new player.
	/// </summary>
	/// <returns>WWW Class to process after in a Coroutine method for get the data.</returns>
	/// <param name="user">The username of the new player.</param>
	/// <param name="password">The password for the new player.</param>
	public WWW register (string user, string password)
	{

		WWWForm form = new WWWForm ();

		form.AddField ("user", user);
		form.AddField ("password", password);

		WWW w = new WWW ("http://labs.iam.cat/~a14sermarbal/DAM/register.php", form);
//		WWW w = new WWW ("http://192.168.1.133/DAM/register.php", form);

		return w;
	}

	/// <summary>
	/// Build the class WWW to get the current user's games done in the history mode.
	/// </summary>
	/// <returns>WWW Class to process after in a Coroutine method for get the data.</returns>
	/// <param name="idUser">User's id</param>
	public WWW getGamesDoneByIdUser (int idUser)
	{

		WWWForm form = new WWWForm ();

		form.AddField ("idUser", idUser);

		WWW w = new WWW ("http://labs.iam.cat/~a14sermarbal/DAM/getGamesDoneByUser.php", form);
//		WWW w = new WWW ("http://192.168.1.133/DAM/getGamesDoneByUser.php", form);

		return w;

	}

	/// <summary>
	/// Build the class WWW to get the global Score (sum of all the max scores in every GameScreen)
	/// </summary>
	/// <returns>WWW Class to process after in a Coroutine method for get the data.</returns>
	/// <param name="min">Score to start.</param>
	/// <param name="max">Score to end.</param>
	public WWW getGlobalScores (int min, int max)
	{

		WWWForm form = new WWWForm ();

		form.AddField ("min", min);
		form.AddField ("max", max);

		WWW w = new WWW ("http://labs.iam.cat/~a14sermarbal/DAM/getGlobalScores.php", form);
//		WWW w = new WWW ("http://192.168.1.133/DAM/getGlobalScores.php", form);

		return w;

	}

	/// <summary>
	/// Build the class WWW to get all scores of a GameScreen.
	/// </summary>
	/// <returns>The all scores by game screen.</returns>
	/// <param name="min">Score to start.</param>
	/// <param name="max">Score to end.</param>
	/// <param name="idGame">The Game's id</param>
	public WWW getAllScoresByGameScreen (int min, int max, int idGame)
	{

		WWWForm form = new WWWForm ();

		form.AddField ("min", min);
		form.AddField ("max", max);
		form.AddField ("idGame", idGame);

		WWW w = new WWW ("http://labs.iam.cat/~a14sermarbal/DAM/getAllScoresByGameScreen.php", form);
//		WWW w = new WWW ("http://192.168.1.133/DAM/getAllScoresByGameScreen.php", form);

		return w;

	}

	/// <summary>
	/// Build the class WWW to get all current user's current scores of a GameScreen.
	/// </summary>
	/// <returns>WWW class with the connection to the scores of the current user.</returns>
	/// <param name="min">Score to start.</param>
	/// <param name="max">Score to end.</param>
	/// <param name="idGame">Identifier game.</param>
	/// <param name="idUser">Identifier user.</param>
	public WWW getScoresByGameScreenByIdUser (int min, int max, int idGame, int idUser)
	{

		WWWForm form = new WWWForm ();
		

		form.AddField ("min", min);
		form.AddField ("max", max);
		form.AddField ("idGame", idGame);
		form.AddField ("idUser", idUser);

		WWW w = new WWW ("http://labs.iam.cat/~a14sermarbal/DAM/getScoresByGameScreenByIdUser.php", form);
//		WWW w = new WWW ("http://192.168.1.133/DAM/getScoresByGameScreenByIdUser.php", form);

		return w;

	}

	/// <summary>
	/// Build the class WWW to insert a score of an user and a GameScreen.
	/// </summary>
	/// <returns>WWW class with the connection to the insertScore php on the server.</returns>
	/// <param name="score">Score to insert.</param>
	/// <param name="idGame">Game id to insert.</param>
	/// <param name="idUser">User's id to insert.</param>
	public WWW insertScore (int score, int idGame, int idUser)
	{

		WWWForm form = new WWWForm ();

		form.AddField ("score", score);
		form.AddField ("idGame", idGame);
		form.AddField ("idUser", idUser);

		WWW w = new WWW ("http://labs.iam.cat/~a14sermarbal/DAM/insertScore.php", form);
//		WWW w = new WWW ("http://192.168.1.133/DAM/insertScore.php", form);

		return w;

	}

	/// <summary>
	/// Inserts the games done relation.
	/// </summary>
	/// <returns>WWW class with the connection to the insert game relation php on the server.</returns>
	/// <param name="idUser">Current id's identifier.</param>
	/// <param name="idGame">Current game's identifier.</param>
	public WWW insertGamesDoneRelation (int idUser, int idGame)
	{

		WWWForm form = new WWWForm ();

		form.AddField ("idUser", idUser);
		form.AddField ("idGame", idGame);

		WWW w = new WWW ("http://labs.iam.cat/~a14sermarbal/DAM/insertGamesDoneRelation.php", form);
//		WWW w = new WWW ("http://192.168.1.133/DAM/insertGamesDoneRelation.php", form);

		return w;
	}

}
