﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Reproduce music and effect sounds.
/// </summary>
public class SoundManager : MonoBehaviour 
{
	// AudioSources classes
	public AudioSource efxSource;
	public AudioSource musicSource;

	// SoundManager instance
	public static SoundManager instance = null;

	// min max pitch range
	private float lowPitchRange = .95f;
	private float highPitchRange = 1.05f;

	// Some sounds used a lot.
	public AudioClip starEffect;
	public AudioClip defaultMusic;
	public AudioClip WinMusic;
	public AudioClip LoseMusic;

	// Array of sounds.
	public AudioClip[] LoseSounds;
	public AudioClip[] WinSounds;

	/// <summary>
	/// Initialize the Sound Manager Class.
	/// </summary>
	void Awake ()
	{
		//Check if there is already an instance of SoundManager
		if (instance == null)
			//if not, set it to this.
			instance = this;
		//If instance already exists:
		else if (instance != this)
			//Destroy this, this enforces our singleton pattern so there can only be one instance of SoundManager.
			Destroy (gameObject);

		//Set SoundManager to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
		DontDestroyOnLoad (gameObject);
	}

	/// <summary>
	/// Used to play single sound clips.
	/// </summary>
	/// <param name="clip">Sound to reproduce.</param>
	public void PlaySingle(AudioClip clip)
	{
		//Set the clip of our efxSource audio source to the clip passed in as a parameter.
		efxSource.clip = clip;

		//Play the clip.
		efxSource.Play ();
	}


	/// <summary>
	/// RandomizeSfx chooses randomly between various audio clips and slightly changes their pitch.
	/// </summary>
	/// <param name="clips">Sounds to reproduce randomly (one of them).</param>
	public void RandomizeSfx (AudioClip[] clips)
	{
		//Generate a random number between 0 and the length of our array of clips passed in.
		int randomIndex = Random.Range(0, clips.Length);

		//Choose a random pitch to play back our clip at between our high and low pitch ranges.
		float randomPitch = Random.Range(lowPitchRange, highPitchRange);

		//Set the pitch of the audio source to the randomly chosen pitch.
		efxSource.pitch = randomPitch;

		//Set the clip to the clip at our randomly chosen index.
		efxSource.clip = clips[randomIndex];

		//Play the clip.
		efxSource.Play();
	}

	/// <summary>
	/// Changes the music.
	/// </summary>
	/// <param name="clip">New music to reproduce.</param>
	public void changeMusic(AudioClip clip){

		musicSource.clip = clip;

		musicSource.Play ();

	}

	/// <summary>
	/// Play the star collect effect.
	/// </summary>
	public void playStarCollect(){
		efxSource.clip = starEffect;

		efxSource.Play ();
	}

	/// <summary>
	/// Play the default music.
	/// </summary>
	public void playDefaultMusic(){
		musicSource.clip = defaultMusic;

		musicSource.Play ();
	}

	/// <summary>
	/// Play the win music.
	/// </summary>
	public void playWinMusic(){
		musicSource.clip = WinMusic;

		musicSource.Play ();
	}

	/// <summary>
	/// Play the lose music.
	/// </summary>
	public void playLoseMusic(){
		musicSource.clip = LoseMusic;

		musicSource.Play ();
	}

	/// <summary>
	/// Play lose sounds (one of them).
	/// </summary>
	public void playLoseSounds(){
		//Generate a random number between 0 and the length of our array of clips passed in.
		int randomIndex = Random.Range(0, LoseSounds.Length);

		//Choose a random pitch to play back our clip at between our high and low pitch ranges.
		float randomPitch = Random.Range(lowPitchRange, highPitchRange);

		//Set the pitch of the audio source to the randomly chosen pitch.
		efxSource.pitch = randomPitch;

		//Set the clip to the clip at our randomly chosen index.
		efxSource.clip = LoseSounds[randomIndex];

		//Play the clip.
		efxSource.Play();
	}

	/// <summary>
	/// Play win sounds (one of them)
	/// </summary>
	public void playWinSounds(){
		//Generate a random number between 0 and the length of our array of clips passed in.
		int randomIndex = Random.Range(0, WinSounds.Length);

		//Choose a random pitch to play back our clip at between our high and low pitch ranges.
		float randomPitch = Random.Range(lowPitchRange, highPitchRange);

		//Set the pitch of the audio source to the randomly chosen pitch.
		efxSource.pitch = randomPitch;

		//Set the clip to the clip at our randomly chosen index.
		efxSource.clip = WinSounds[randomIndex];

		//Play the clip.
		efxSource.Play();
	}



}