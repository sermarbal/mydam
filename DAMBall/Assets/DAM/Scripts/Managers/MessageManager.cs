﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Create messages for send data to the user.
/// </summary>
public class MessageManager : MonoBehaviour
{
	// MessageManager instance
	public static MessageManager instance = null;

	// Canvas 
	public Canvas messageCanvas;

	/// <summary>
	/// Initialize the MessageManager
	/// </summary>
	void Awake ()
	{
		
		//Check if there is already an instance of MenuManager
		if (instance == null)
			//if not, set it to this.
			instance = this;
		//If instance already exists:
		else if (instance != this)
			//Destroy this, this enforces our singleton pattern so there can only be one instance of MenuManager.
			Destroy (gameObject);

		//Set MenuManager to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
		DontDestroyOnLoad (gameObject);

	}

	/// <summary>
	/// Disable the Message
	/// </summary>
	void Start(){
		messageCanvas.enabled = false;
	}

	/// <summary>
	/// Show Message in the screen.
	/// </summary>
	/// <param name="message">String to show in the message.</param>
	public void showMessage (string message)
	{
		// Enable Message Canvas
		messageCanvas.enabled = true;
		// Show Message
		messageCanvas.GetComponentInChildren<Text> ().text = message;
	}

	/// <summary>
	/// Show Message in the screen for some seconds.
	/// </summary>
	/// <param name="seconds">Duration in seconds.</param>
	/// <param name="message">String to show in the message.</param>
	public void showMessage (float seconds, string message)
	{
		// Starts the thread to display the message.
		StartCoroutine(MessageSeconds(seconds, message));
	}

	/// <summary>
	/// Coroutine that shows the message during x seconds.
	/// </summary>
	/// <returns>The seconds.</returns>
	/// <param name="seconds">Duration in seconds.</param>
	/// <param name="message">String to show in the message.</param>
	public IEnumerator MessageSeconds(float seconds, string message)
	{
		messageCanvas.enabled = true;
		messageCanvas.GetComponentInChildren<Text> ().text = message;
		yield return new WaitForSeconds(seconds);
		messageCanvas.enabled = false;
	}

	/// <summary>
	/// Close the Message panel.
	/// </summary>
	public void closeMessage(){
		messageCanvas.enabled = false;
	}

}
