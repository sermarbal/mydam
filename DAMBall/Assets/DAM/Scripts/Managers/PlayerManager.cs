﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Contains data for manage the game using the current player.
/// </summary>
public class PlayerManager : MonoBehaviour {

	public static PlayerManager instance;
	public int id = 0;
	public string username = null;
	// Difficulty: 0 none, 1 Easy, 2 normal, 3 Hard
	public int difficulty = 0;
	// Game mode: 0 none, 1 History, 2 Free
	public int gameMode = 0;

	/// <summary>
	/// Awake this instance.
	/// </summary>
	void Awake ()
	{
		//Check if there is already an instance of PlayerManager
		if (instance == null)
			//if not, set it to this.
			instance = this;
		//If instance already exists:
		else if (instance != this)
			//Destroy this, this enforces our singleton pattern so there can only be one instance of PlayerManager.
			Destroy (gameObject);
		//Set PlayerManager to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
		DontDestroyOnLoad (gameObject);
	}

}
