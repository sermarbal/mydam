﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

/// <summary>
/// Manages the current game information and contains global methods for the game.
/// </summary>
public class ScreenManager : MonoBehaviour
{

	// Instance of the class
	public static ScreenManager instance;

	// Canvas
	public Canvas CanvasWinner;
	public Canvas CanvasLoser;
	public Canvas CanvasInfo;

	// GameObject Player
	public GameObject player;

	// stars in the current game
	private GameObject[] stars;
	// stars collected by the player
	private int starsCollected;

	// max time in the game
	private float maxTime = 360;
	// time pased during the current game
	private float cont = 0;

	// score for second
	private int scoreSecond = 10;
	// score for star collected
	private int scoreStar = 1000;
	// score
	private int score;

	// player limit position Y
	private float limit = -110f;

	// button clic effect
	public AudioClip buttonClicEffect;

	// bool needed to control the times entered to lose by time.
	private bool loseByTimeEntered = false;

	/// <summary>
	/// Awake this instance.
	/// </summary>
	void Awake ()
	{
		//Check if there is already an instance of ScreenManager
		if (instance == null)
			//if not, set it to this.
			instance = this;
		//If instance already exists:
		else if (instance != this)
			//Destroy this, this enforces our singleton pattern so there can only be one instance of ScreenManager.
			Destroy (gameObject);
	}

	/// <summary>
	/// Initialize the class.
	/// </summary>
	void Start ()
	{

		// Disable the win/lose canvas
		CanvasWinner.enabled = false;
		CanvasLoser.enabled = false;

		// Find all stars in the current game and updates the stars array.
		stars = GameObject.FindGameObjectsWithTag ("Star");

		// initialize the starsCollected variable to 0.
		starsCollected = 0;

		// Update the stars canvas to 0/x stars collected. 
		((Text)CanvasInfo.transform.Find ("StarsText").GetComponent<Text> ()).text = "Stars: " + starsCollected + "/" + stars.Length;

		// Update the score value depending the difficulty.
		if (PlayerManager.instance.difficulty == 1) {
			scoreSecond = 10;
			scoreStar = 1000;
		} else if (PlayerManager.instance.difficulty == 2) {
			scoreSecond = 100;
			scoreStar = 10000;
		} else if (PlayerManager.instance.difficulty == 3) {
			scoreSecond = 1000;
			scoreStar = 100000;
		}

		// Win Buttons Addlisteners
		CanvasWinner.transform.Find ("BtnRestart").GetComponent<Button> ().onClick.AddListener (() => onClickRestartWin ());
		CanvasWinner.transform.Find ("SaveScoreBtnRestart").GetComponent<Button> ().onClick.AddListener (() => onClickSaveScoreandRestart ());

		CanvasWinner.transform.Find ("BtnExit").GetComponent<Button> ().onClick.AddListener (() => onClickExitWin ());
		CanvasWinner.transform.Find ("SaveScoreBtnExit").GetComponent<Button> ().onClick.AddListener (() => onClickSaveScoreandExit ());


		// Lose Buttons Addlisteners
		CanvasLoser.transform.Find ("BtnRestart").GetComponent<Button> ().onClick.AddListener (() => onClickRestartLose ());
		CanvasLoser.transform.Find ("BtnExit").GetComponent<Button> ().onClick.AddListener (() => onClickExitLose ());

	}

	// Update is called once per frame
	void Update ()
	{
		cont += Time.deltaTime;

		// Looks if the counter is more than maxTime and execute the loseByTime.
		if (cont >= maxTime && !loseByTimeEntered) {
			// boolean for do that it don't repeat every update.
			loseByTimeEntered = true;
			// LoseByTime function's call.
			playerLose ();
		} else
			// Updates the time text in the canvasPlaying
			CanvasInfo.transform.Find ("TimeText").GetComponent<Text> ().text = "Time: " + (int)(maxTime - cont);
	}

	private void disableButtons ()
	{
		// Win Buttons disable
		CanvasWinner.transform.Find ("BtnRestart").GetComponent<Button> ().interactable = false;
		CanvasWinner.transform.Find ("SaveScoreBtnRestart").GetComponent<Button> ().interactable = false;

		CanvasWinner.transform.Find ("BtnExit").GetComponent<Button> ().interactable = false;
		CanvasWinner.transform.Find ("SaveScoreBtnExit").GetComponent<Button> ().interactable = false;

		// Lose Buttons disable
		CanvasLoser.transform.Find ("BtnRestart").GetComponent<Button> ().interactable = false;
		CanvasLoser.transform.Find ("BtnExit").GetComponent<Button> ().interactable = false;
	}

	/// <summary>
	/// Player win method
	/// </summary>
	public void playerWin ()
	{
		
		// Destroy the script ball controller
		Destroy (player.GetComponent<BallController> ());
		// Insert the script winner to the ball, (it push up the ball)
		player.AddComponent<Winner> ();

		// Play the win sounds
		SoundManager.instance.playWinMusic ();
		SoundManager.instance.playWinSounds ();

		// Update the canvas win with the correct values
		ScreenManager.instance.updateWinCanvas ();

		// Disable canvas info and enable canvas win
		CanvasInfo.enabled = false;
		CanvasWinner.enabled = true;
	}

	/// <summary>
	/// Players lose method
	/// </summary>
	public void playerLose ()
	{
		// Play sounds
		SoundManager.instance.playLoseMusic ();
		SoundManager.instance.playLoseSounds ();
		// Change canvas
		CanvasInfo.enabled = false;
		CanvasLoser.enabled = true;
		// Destroy player
		Destroy (player);
	}

	/// <summary>
	/// Looks if the player fell under the lose line
	/// </summary>
	public void checkPlayerPosition ()
	{
		if (player.transform.position.y < limit) {
			ScreenManager.instance.playerLose ();
		}
	}

	/// <summary>
	/// Method called when OnTriggerEnter of an star is activated.
	/// </summary>
	/// <param name="currentStar">Current star.</param>
	public void addStar (GameObject currentStar)
	{
		// Play the star effect
		SoundManager.instance.playStarCollect ();
		// Increase the stars counter.
		starsCollected++;
		((Text)CanvasInfo.transform.Find ("StarsText").GetComponent<Text> ()).text = "Stars: " + starsCollected + "/" + stars.Length;
	}



	/// <summary>
	/// Save score and restart the game.
	/// </summary>
	void onClickSaveScoreandRestart ()
	{
		SoundManager.instance.PlaySingle (buttonClicEffect);
		SoundManager.instance.playDefaultMusic ();
		// dissable buttons
		disableButtons ();
		if (PlayerManager.instance.gameMode == 1) {
			WWW w = DAOManager.instance.insertGamesDoneRelation (PlayerManager.instance.id, SceneManager.GetActiveScene ().buildIndex);
			StartCoroutine (processGamesDoneRestartWWW (w));
		} else {
			WWW w = DAOManager.instance.insertScore (score, SceneManager.GetActiveScene ().buildIndex, PlayerManager.instance.id);
			StartCoroutine (processRestartWWW (w));
		}
	}

	/// <summary>
	/// Restart the game.
	/// </summary>
	void onClickRestartWin ()
	{
		SoundManager.instance.PlaySingle (buttonClicEffect);
		SoundManager.instance.playDefaultMusic ();
		// dissable buttons
		disableButtons ();
		if (PlayerManager.instance.gameMode == 1) {
			WWW w = DAOManager.instance.insertGamesDoneRelation (PlayerManager.instance.id, SceneManager.GetActiveScene ().buildIndex);
			StartCoroutine (processGamesDoneRestartWWW (w));
		} else {
			SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
		}
	}

	/// <summary>
	/// Save score and exit to menu scene.
	/// </summary>
	void onClickSaveScoreandExit ()
	{
		SoundManager.instance.PlaySingle (buttonClicEffect);
		SoundManager.instance.playDefaultMusic ();
		// dissable buttons
		disableButtons ();
		if (PlayerManager.instance.gameMode == 1) {
			WWW w = DAOManager.instance.insertGamesDoneRelation (PlayerManager.instance.id, SceneManager.GetActiveScene ().buildIndex);
			StartCoroutine (processGamesDoneExitWWW (w));
		} else {
			WWW w = DAOManager.instance.insertScore (score, SceneManager.GetActiveScene ().buildIndex, PlayerManager.instance.id);
			StartCoroutine (processExitWWW (w));
		}
	}

	/// <summary>
	/// Exit to menu scene by win canvas.
	/// </summary>
	void onClickExitWin ()
	{
		SoundManager.instance.PlaySingle (buttonClicEffect);
		SoundManager.instance.playDefaultMusic ();
		// dissable buttons
		disableButtons ();
		if (PlayerManager.instance.gameMode == 1) {
			WWW w = DAOManager.instance.insertGamesDoneRelation (PlayerManager.instance.id, SceneManager.GetActiveScene ().buildIndex);
			StartCoroutine (processGamesDoneExitWWW (w));
		} else {
			SceneManager.LoadScene ("Menus");
		}
	}

	/// <summary>
	/// Restart the game by lose canvas.
	/// </summary>
	void onClickRestartLose ()
	{
		SoundManager.instance.PlaySingle (buttonClicEffect);
		SoundManager.instance.playDefaultMusic ();
		// dissable buttons
		disableButtons ();
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);

	}

	/// <summary>
	/// Exit to menu scene.
	/// </summary>
	void onClickExitLose ()
	{
		SoundManager.instance.PlaySingle (buttonClicEffect);
		SoundManager.instance.playDefaultMusic ();
		// dissable buttons
		disableButtons ();
		SceneManager.LoadScene ("Menus");

	}

	/// <summary>
	/// Update the win canvas data.
	/// </summary>
	public void updateWinCanvas ()
	{
		// Time
		CanvasWinner.transform.Find ("Time").GetComponent<Text> ().text = "Time: " + (int)(maxTime - cont);

		// Score
		score = (int)(maxTime - cont);
		score *= scoreSecond;
		score += starsCollected * scoreStar;
		CanvasWinner.transform.Find ("Score").GetComponent<Text> ().text = "Score: " + score;

		// Stars
		CanvasWinner.transform.Find ("Stars").GetComponent<Text> ().text = "Stars: " + starsCollected + "/" + stars.Length;

	}

	/// <summary>
	/// Process the restart thread saving the score.
	/// </summary>
	/// <returns>Get the w data from internal Coroutine class</returns>
	/// <param name="w">WWW class with the connection to the php class for insert the score</param>
	IEnumerator processRestartWWW (WWW w)
	{
		// Show message logging... while we wait the response of the server.
		MessageManager.instance.showMessage ("Uploading Scoreasdasdasd...");
		// if we receibe the response it continues after this point.
		yield return w;
		// We have the response so the message "Recording..." is close here.
		MessageManager.instance.closeMessage ();

		// Control if we have an error in the response of the server.
		if (w.error == null) {
			// Split the response for get the data.
			string[] arrayResponse = w.text.Split ('#');
			// If the array[0] is 0 the response is not ok and is ok if it is 1
			if (arrayResponse [0].Equals ("1")) {
				
				// Show OK Message
				MessageManager.instance.showMessage (3, arrayResponse [1]);

				// Finally restart using the OnClickRestart function.
				SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);

				// If array[0] is 0 the response is not ok and we show the response
			} else if (arrayResponse [0].Equals ("0")) {
				print ("0#");
				MessageManager.instance.showMessage (3, arrayResponse [1]);
			} 

		} else {
			if (Application.internetReachability == NetworkReachability.NotReachable) {
				MessageManager.instance.showMessage (3, "Error. Check internet connection!");
			} else {
				// if an error ocurred we show the connection error
				MessageManager.instance.showMessage (3, w.error);
			}
		}
	}

	/// <summary>
	/// Processes the exit thread saving the score.
	/// </summary>
	/// <returns>Get the w data from internal Coroutine class</returns>
	/// <param name="w">WWW class with the connection to the php class for insert the score</param>
	IEnumerator processExitWWW (WWW w)
	{
		// Show message logging... while we wait the response of the server.
		MessageManager.instance.showMessage ("Uploading Scoreasdasd...");
		// if we receibe the response it continues after this point.
		yield return w;
		// We have the response so the message "Recording..." is close here.
		MessageManager.instance.closeMessage ();

		// Control if we have an error in the response of the server.
		if (w.error == null) {
			// Split the response for get the data.
			string[] arrayResponse = w.text.Split ('#');
			// If the array[0] is 0 the response is not ok and is ok if it is 1
			if (arrayResponse [0].Equals ("1")) {

				// Show OK Message
				MessageManager.instance.showMessage (3, arrayResponse [1]);

				// If array[0] is 0 the response is not ok and we show the response
			} else if (arrayResponse [0].Equals ("0")) {
				print ("0#");
				MessageManager.instance.showMessage (3, arrayResponse [1]);
			}
			// Go to Menus scene.
			SceneManager.LoadScene ("Menus");
		} else {
			if (Application.internetReachability == NetworkReachability.NotReachable) {
				MessageManager.instance.showMessage (3, "Error. Check internet connection!");
			} else {
				// if an error ocurred we show the connection error
				MessageManager.instance.showMessage (3, w.error);
			}
		}


	}

	/// <summary>
	/// Processes the GamesDone thread relation and restart.
	/// </summary>
	/// <returns>Get the w data from internal Coroutine class</returns>
	/// <param name="w">WWW class with the connection to the php class for update the games done</param>
	IEnumerator processGamesDoneRestartWWW (WWW w)
	{
		// Show message logging... while we wait the response of the server.
		MessageManager.instance.showMessage ("Updating Games done...");
		// if we receibe the response it continues after this point.
		yield return w;
		// We have the response so the message "Recording..." is close here.
		MessageManager.instance.closeMessage ();

		// Control if we have an error in the response of the server.
		if (w.error == null) {
			// Split the response for get the data.
			string[] arrayResponse = w.text.Split ('#');
			// If the array[0] is 0 the response is not ok and is ok if it is 1
			if (arrayResponse [0].Equals ("1")) {

				// Show OK Message
				MessageManager.instance.showMessage (3, arrayResponse [1]);

				// If array[0] is 0 the response is not ok and we show the response
			} else if (arrayResponse [0].Equals ("0")) {
//				print ("0#"+arrayResponse [1]);
//				MessageManager.instance.showMessageSeconds (3, arrayResponse [1]);
			}
			// Create the new thread to save the score.
			WWW w2 = DAOManager.instance.insertScore (score, SceneManager.GetActiveScene ().buildIndex, PlayerManager.instance.id);
			StartCoroutine (processRestartWWW (w2));
		} else {
			if (Application.internetReachability == NetworkReachability.NotReachable) {
				MessageManager.instance.showMessage (3, "Error. Check internet connection!");
			} else {
				// if an error ocurred we show the connection error
				MessageManager.instance.showMessage (3, w.error);
			}
		}


	}

	/// <summary>
	/// Processes the GamesDone thread relation and exit.
	/// </summary>
	/// <returns>Get the w data from internal Coroutine class</returns>
	/// <param name="w">WWW class with the connection to the php class for update the games done</param>
	IEnumerator processGamesDoneExitWWW (WWW w)
	{
		// Show message logging... while we wait the response of the server.
		MessageManager.instance.showMessage ("Updating Games done...");
		// if we receibe the response it continues after this point.
		yield return w;
		// We have the response so the message "Recording..." is close here.
		MessageManager.instance.closeMessage ();

		// Control if we have an error in the response of the server.
		if (w.error == null) {
			// Split the response for get the data.
			string[] arrayResponse = w.text.Split ('#');
			// If the array[0] is 0 the response is not ok and is ok if it is 1
			if (arrayResponse [0].Equals ("1")) {

				// Show OK Message
				MessageManager.instance.showMessage (3, arrayResponse [1]);

				// If array[0] is 0 the response is not ok and we show the response
			} else if (arrayResponse [0].Equals ("0")) {
			// print ("0#"+arrayResponse [1]);
			// MessageManager.instance.showMessageSeconds (3, arrayResponse [1]);
			}
			// Create the new thread to save the score.
			WWW w2 = DAOManager.instance.insertScore (score, SceneManager.GetActiveScene ().buildIndex, PlayerManager.instance.id);
			StartCoroutine (processExitWWW (w2));
		} else {
			if (Application.internetReachability == NetworkReachability.NotReachable) {
				MessageManager.instance.showMessage (3, "Error. Check internet connection!");
			} else {
				// if an error ocurred we show the connection error
				MessageManager.instance.showMessage (3, w.error);
			}
		}

	}

}
