﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindByComponent : MonoBehaviour
{

//	public Light LightFound;
	public Light[] LightFound;

//	void Start ()
//	{
//		
//		LightFound = FindObjectOfType (typeof(Light)) as Light;
//
//		if (LightFound != null) {
//			print ("Se ha encontrado el objeto de tipo Light");
//		} else {
//			print ("No se encontraron obetos de tipo Light");
//		}
//
//	}

	void Start () {
		LightFound = FindObjectsOfType (typeof(Light)) as Light[ ];

		if (LightFound.Length > 0) {
			print ("Se han encontrado objetos de tipo Light!");
		} else {
			print ("No se encontró ningún objeto de tipo Light"); 
		}
	}
}