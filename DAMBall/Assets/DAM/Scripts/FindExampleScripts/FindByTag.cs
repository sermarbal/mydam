﻿using UnityEngine;
using System.Collections;

public class FindByTag : MonoBehaviour
{
	// public GameObject found;
	public GameObject[ ] found;

//	void Start ( ) {
//		found = GameObject.FindWithTag ( "CubeTestTag" ); if ( found != null ) {
//		print ( "Se ha encontrado el objeto!" ); } else {
//			print ( "No existe el objeto..." ); }
//	}

	void Update ()
	{
		found = GameObject.FindGameObjectsWithTag (
			"Bullet");
		if (found.Length > 0) {
			print ("Se han encontrado "+found.Length+" objetos!");
		} else {
			print ("No existen objetos con esa tag...");
		}
	}

}