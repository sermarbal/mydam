﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindHierarchyByName : MonoBehaviour
{
	
	public Transform sword;

	void Start ()
	{
		sword = transform.Find ("Sword");
		if (sword != null) {
			print ("Se ha encontrado la espada: "+sword.ToString());
		} else {
			print ("No se ha encontrado el filo de la espada");
		}
	}

}
