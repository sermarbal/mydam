﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TestException : Exception {

	public TestException(){
	}

	public TestException(string message) : base(message){
		
	}

	public TestException(string message, Exception inner)
		: base(message, inner)
	{
	}

}
