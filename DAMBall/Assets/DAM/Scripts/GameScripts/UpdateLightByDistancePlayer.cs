﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Updates the lights by the distance player.
/// </summary>
public class UpdateLightByDistancePlayer : MonoBehaviour {

	// Player gameobject
	public GameObject player;
	// Limits intensity
	public float maxIntensity = 100;
	public float minIntensity = 10;
	// Increment intensity for interval.
	public float incrementIntensity = 1;
	// interval intensity
	public float intervalIntensity = 0.1f;

	private float metters = 5;
	// counter
	private float cont;

	// Update is called once per frame
	void Update () {

		// if the player is not referenced it will not executed.
		if (player != null) {

			// Distance to the ball
			float distance = Vector3.Distance (player.transform.position, transform.position);
			// Update the counter
			cont += Time.deltaTime;

			// Get the current intensity
			float i = GetComponent<Light> ().intensity;

			// If the distance between the light is less than the required, increment the intensity when the interval allows
			if (distance < metters && i < maxIntensity) {
				GetComponent<Light> ().intensity += incrementIntensity;
				cont = 0;
			} else if (distance >= metters && i > minIntensity) {
				GetComponent<Light> ().intensity -= incrementIntensity;
				cont = 0;
			}
		}


	}
}
