﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Update constraints of the target using the predefined constraints.
/// </summary>
public class UpdateConstraints : MonoBehaviour {

	// Constraint (declared in the unity interface)
	public RigidbodyConstraints constrait;

	/// <summary>
	/// Raises the trigger enter event.
	/// </summary>
	/// <param name="reference">Reference.</param>
	void OnTriggerEnter(Collider reference){
		// Add the constraint to the reference.
		reference.GetComponent<Rigidbody>().constraints = constrait;
	}

}
