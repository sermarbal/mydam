﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Star trigger script.
/// </summary>
public class StarTriggerScript : MonoBehaviour {

	// Reference to player ball.
	public GameObject player;

	/// <summary>
	/// Raises the trigger enter event.
	/// </summary>
	/// <param name="target">Target.</param>
	void OnTriggerEnter(Collider target) {
		// looks if the reference is the player
		if (target.gameObject == player.gameObject) {
			// Add the star
			ScreenManager.instance.addStar (gameObject);
			// Destroy the star
			Destroy (gameObject);
		}
	}

}
