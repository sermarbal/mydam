﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Rotate machine using speed rotation
/// </summary>
public class RotateMachine : MonoBehaviour {

	// speed rotation
	public float speed = 1;

	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update(){
		// Add the rotation using the speed predefined.
		transform.Rotate (Vector3.up* speed *Time.deltaTime);
	}

}
