﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Destroy parent by trigger.
/// </summary>
public class DestroyParentByTrigger : MonoBehaviour {

	/// <summary>
	/// Raises the trigger enter event.
	/// </summary>
	/// <param name="reference"></param>
	void OnTriggerEnter(Collider reference){
		// Destroy the parent
		Destroy(transform.parent.gameObject);
	}
}
