﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Rotate the current object
/// </summary>
public class Rotate : MonoBehaviour {
	
	// Update is called once per frame
	void Update () {
		// Add the rotation
		transform.Rotate (new Vector3 (0, 0, 30) * Time.deltaTime);	
	}

}
