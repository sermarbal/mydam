﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Initialize an object when the GameObject that contains it is destroyed.
/// </summary>
public class InitializeOnDestroy : MonoBehaviour
{

	// Object to initialize
	public GameObject objectToInitialize;

	// Method called when the current object is destroyed.
	void OnDestroy ()
	{
		// Method to initialize a new object.
		Instantiate (objectToInitialize, transform.position, transform.rotation);
	}
}
