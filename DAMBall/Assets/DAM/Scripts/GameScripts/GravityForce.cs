﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Gravity force script.
/// </summary>
public class GravityForce : MonoBehaviour
{

	// Force to add
	public float force;

	/// <summary>
	/// Raises the trigger enter event.
	/// </summary>
	/// <param name="reference">Reference.</param>
	void OnTriggerEnter(Collider reference){
		// cleans the material of the ball
		reference.gameObject.GetComponent<Collider> ().material = null;

	}

	/// <summary>
	/// Raises the trigger stay event.
	/// </summary>
	/// <param name="reference">Reference.</param>
	void OnTriggerStay (Collider reference)
	{
		// If the collision is with the ball
		if (reference.name.Equals ("PlayerBall")) {
			// Takes the rigidbody component and add the force.
			Rigidbody rb = reference.gameObject.GetComponent<Rigidbody> ();
			rb.AddForce (Vector3.up * Time.deltaTime * force, ForceMode.Acceleration);
		}
	}



}
