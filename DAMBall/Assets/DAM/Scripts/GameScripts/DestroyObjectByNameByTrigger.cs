﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Destroy the object using his name.
/// </summary>
public class DestroyObjectByNameByTrigger : MonoBehaviour {

	// Object name to destroy
	public string nameObject = "DoorTest2";

	/// <summary>
	/// Raises the trigger enter event.
	/// </summary>
	/// <param name="reference">collision object</param>
	void OnTriggerEnter(Collider reference){

		// Get the object to destroy
		GameObject door = GameObject.Find (nameObject);
		// Destroy the object
		Destroy (door);

	}

}
