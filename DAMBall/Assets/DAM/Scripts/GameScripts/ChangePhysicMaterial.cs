﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script that changes the phsysic material when an object enter on the trigger and return the old physic material when leaves.
/// </summary>
public class ChangePhysicMaterial : MonoBehaviour
{

	// Physic material to insert when the object enter on the trigger.
	public PhysicMaterial physicMaterial;
	// Old physic material to return when the object leaves.
	public PhysicMaterial defaultMaterial = null;

	/// <summary>
	/// Raises the trigger enter event.
	/// </summary>
	/// <param name="reference">Object to change the physic material.</param>
	void OnTriggerEnter (Collider reference)
	{
		// saves the current material (old material after)
		defaultMaterial = reference.material;
		// update the physic material.
		reference.material = physicMaterial;
	}

	/// <summary>
	/// Raises the trigger exit event.
	/// </summary>
	/// <param name="reference">Return the physic material.</param>
	void OnTriggerExit (Collider reference)
	{

		// Returns the old physic material to the reference.
		reference.material = defaultMaterial;
	
	}

}
