﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Activate the win canvas using the ScreenManager playerWin() method.
/// </summary>
public class PlayerWin : MonoBehaviour {

	/// <summary>
	/// Raises the trigger enter event.
	/// </summary>
	/// <param name="reference">Reference.</param>
	void OnTriggerEnter(Collider reference){
		// Trigger the win function
		ScreenManager.instance.playerWin ();

	}

}
