﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Add up force to the winner ball.
/// </summary>
public class Winner : MonoBehaviour
{
	// Force to add
	public float force = 750;

	/// <summary>
	/// Fixeds the update.
	/// </summary>
	void FixedUpdate ()
	{
		// Add force to up to rigidbody
		transform.GetComponent<Rigidbody> ().AddForce (Vector3.up * force * Time.deltaTime, ForceMode.Acceleration);
	}

}
