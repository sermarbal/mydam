﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Orbite gameobject to target
/// </summary>
public class Orbite : MonoBehaviour {

	// Target to rotate
	public GameObject target;

	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update () {
		// Get the target transform
		Transform targetTransform = target.transform;
		// Looks to the target
		transform.LookAt (targetTransform);
		// Move the object in local space.
		transform.Translate (Vector3.right, Space.Self);

	}
}
