﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Teleports the triggerEnterReference to destine point.
/// </summary>
public class TeleportTarget : MonoBehaviour {

	// Name of the empty object where the reference will be teleported.
	public string destinePoint;

	/// <summary>
	/// Raises the trigger enter event.
	/// </summary>
	/// <param name="reference">Player ball to teletransport</param>
	void OnTriggerEnter(Collider reference){

		// if the trigger object entered is the player ball teleport it to the destine point.
		if (reference.gameObject.name.Equals ("PlayerBall")) {
			// Take the object by name.
			GameObject tp1 = GameObject.Find(destinePoint);
			// Teleport the target to the new point using the position vector.
			reference.transform.position = tp1.transform.position;
		}

	}

}
