﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Enemy destroy the ball and finish the game losing it.
/// </summary>
public class EnemyKillPlayer : MonoBehaviour
{

	// Player ball GameObject
	public GameObject player;

	/// <summary>
	/// On collision destroy the player
	/// </summary>
	/// <param name="reference">Reference.</param>
	void OnCollisionEnter (Collision reference)
	{
		if (player == reference.gameObject)
			ScreenManager.instance.playerLose ();
	}

}
