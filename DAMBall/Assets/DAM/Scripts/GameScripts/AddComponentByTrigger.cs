﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Add TeleportTarget and UpdateConstraint scripts to the target.
/// </summary>
public class AddComponentByTrigger : MonoBehaviour {

	// Target to insert the scripts.
	public GameObject target;
	// Tag name of the element that activates the teleporter.
	public string tagElement;

	// Script that teleports a target to a new location.
	private TeleportTarget teleportTarget;
	// Script that changes the enabled directionContraints.
	private UpdateConstraints updateConstraints;

	/// <summary>
	/// Raises the trigger enter event.
	/// </summary>
	/// <param name="reference">Player ball</param>
	void OnTriggerEnter(Collider reference){

		// if the collider reference has the tagname defined previously...
		if (reference.gameObject.tag.Equals (tagElement)) {

			// Add Teleport script to the GameObject.
			teleportTarget = target.AddComponent<TeleportTarget> ();
			// Update value of the new object to the name of the destination point.
			teleportTarget.destinePoint="Teleport point 1";

			// Add constraints script to the GameObject.
			updateConstraints = target.AddComponent<UpdateConstraints> ();
			// Update the constraint to freeze.
			updateConstraints.constrait = RigidbodyConstraints.FreezePositionX;

		}

	}

	/// <summary>
	/// Raises the trigger exit event.
	/// </summary>
	/// <param name="reference">Collider reference</param>
	void OnTriggerExit(Collider reference){

		// If the object referenced with the tag correctly leaves the zone...
		if (reference.gameObject.tag.Equals (tagElement)) {
			// Destroy Teleport script
			Destroy(target.gameObject.GetComponent<TeleportTarget> ());
			// Destroy Constraint script
			Destroy(target.gameObject.GetComponent<UpdateConstraints> ());
		}

	}

}
