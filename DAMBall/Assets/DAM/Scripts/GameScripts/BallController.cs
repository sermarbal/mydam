﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Controlle the ball controls
/// </summary>
public class BallController : MonoBehaviour
{
	// Camera reference
	public Camera Reference;
	// MovementPoint GameObject reference
	public GameObject MovementSupportPoint;

	// Movement ball forces
	public float moveForce = 25;
	public float rotateForce = 25;
	public float jumpForce = 50;
	public float groundDistance = 0.1f;

	// String references to InputManager internal Unity Class
	public string horizontalMoveController = "Horizontal";
	public string verticalMoveController = "Vertical";
	public string jumpController = "Jump";

	// Canvas win/lose
	public Canvas winCanvas;
	public Canvas loseCanvas;

	// AudioClips that contain the sounds of collisions.
	public AudioClip[] CollisionSound;

	// Explosion GameObject prefab
	public GameObject Explosion;

	void FixedUpdate ()
	{
		// Take this RigidBody
		Rigidbody rb = GetComponent<Rigidbody> ();

		float input;
		float axis;
		Vector3 result;

		// MovementSupportPoint look at the ball
		MovementSupportPoint.transform.LookAt (transform);

		// define the new position of the MovementSupportPoint
		Vector3 newPosition = new Vector3 ();
		newPosition.x = Reference.transform.position.x;
		newPosition.z = Reference.transform.position.z;
		newPosition.y = transform.position.y;
		MovementSupportPoint.transform.position = newPosition;

		// W S force

		// Get input force
		input = Input.GetAxis (verticalMoveController);
		axis = input * moveForce * Time.deltaTime;
		result = MovementSupportPoint.transform.forward * moveForce * axis;
		// Freeze the y force.
		result.y = 0f;
		// Add the force
		rb.AddForce (result, ForceMode.Acceleration);

		// A D force

		// Get input force
		input = Input.GetAxis (horizontalMoveController);
		axis = input * moveForce * Time.deltaTime;
		result = Reference.transform.right * moveForce * axis;
		// Freeze Y force
		result.y = 0f;

		// Add the force
		rb.AddForce (result, ForceMode.Acceleration);

		// looks if the ball is touching the ground.
		if (isGrounded ()) {
			// Get axis by InputManager internal class unity.
			input = Input.GetAxis (jumpController);
			axis = input * jumpForce * Time.deltaTime;
			// Add the force
			rb.AddForce (Vector3.up * jumpForce * axis, ForceMode.Acceleration);
		}

		// Check if the player ball is under the limit line. if he is, he lose.
		ScreenManager.instance.checkPlayerPosition ();

	}

	/// <summary>
	/// Executed when the ball has a collision with another object.
	/// </summary>
	/// <param name="reference">Reference.</param>
	private void OnCollisionEnter (Collision reference){
		SoundManager.instance.RandomizeSfx (CollisionSound);
	}

	/// <summary>
	/// Determines whether this instance is grounded.
	/// </summary>
	/// <returns><c>true</c> if this instance is grounded; otherwise, <c>false</c>.</returns>
	private bool isGrounded ()
	{
		// Use a raycast casted below the ball
		return Physics.Raycast (transform.position, -Vector3.up, groundDistance + 0.5f);
	}

}
